const Version = '!2.34.1!'.replace(/!/g, ''); // x-release-please-version

export default Version;
